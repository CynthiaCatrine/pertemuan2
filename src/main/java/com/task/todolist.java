package com.task;

import java.util.ArrayList;

public class todolist {
    private ArrayList<task> todo;

    public todolist(){
        this.todo = new ArrayList<>();
    }

    public void addTodo(int id, String name, String status){
        todo.add(new task(id, name, status));
    }

    public String getTodo(){
        String temp = "";

        for (int i=0; i<todo.size(); i++){
            temp += todo.get(i).getTask();

            if(i<todo.size()-1) {
                temp += "\n";
            }
        }
        return temp;
     }
}
