package com.task;

import java.util.ArrayList;

public class task {
    private int id;
    private String name;
    private String status;

    public task(int id, String name, String status){
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public String getTask(){
        return id + ". " + this.name + " " + "[" + this.status + "]";
    }

    public boolean checkStatus(){
        return status == "DONE" || status == "NOT DONE";
    }
}
