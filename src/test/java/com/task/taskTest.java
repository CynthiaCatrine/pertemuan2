package com.task;

import org.junit.Test;

import static org.junit.Assert.*;

public class taskTest {

    @Test
    public void TestAddTask(){
        String expectedTask = "1. Coding [DONE]";

        task Task = new task(1, "Coding", "DONE");

        assertEquals(expectedTask, Task.getTask());
    }

    @Test
    public void checkStatus(){
        task Task = new task(1, "Coding", "DONE");

        assertTrue(Task.checkStatus());
    }
}