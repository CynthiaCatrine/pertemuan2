package com.task;

import org.junit.Test;

import static org.junit.Assert.*;

public class todolistTest {

    @Test
    public void testAddTodolist(){

        todolist todo = new todolist();

        String expectedTodo = "1. Coding [DONE]\n2. Eat [DONE]\n3. Hangout [NOT DONE]";

        todo.addTodo(1, "Coding", "DONE");
        todo.addTodo(2, "Eat", "DONE");
        todo.addTodo(3, "Hangout", "NOT DONE");

        assertEquals(expectedTodo, todo.getTodo());
    }
}